FROM ubuntu:latest
MAINTAINER Sébastien Gilles "sebastien.gilles@inria.fr"

RUN (apt-get update && apt-get upgrade -y -q && apt-get dist-upgrade -y -q && apt-get -y -q autoclean && apt-get -y -q autoremove)

RUN apt-get install --no-install-recommends -y inkscape &&\
    rm -rf /var/cache/apk/*
    
ENTRYPOINT ["inkscape"]    
    
